CREATE TABLE snippets(id serial not null unique, title varchar(100) not null, content text not null, created date not null, expires date not null);
CREATE INDEX idx_snippets_created ON snippets(created);
INSERT INTO snippets (title, content, created, expires) VALUES ('An old silent pond',
'An old silent pond... A frog jumps into the pond,splash! Silence again.  -Matsuo Basho',
CURRENT_DATE, CURRENT_DATE + INTERVAL '365 day');
